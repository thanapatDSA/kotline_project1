package dv.kotlintspring.kotline

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlineApplication

fun main(args: Array<String>) {
    runApplication<KotlineApplication>(*args)
}
