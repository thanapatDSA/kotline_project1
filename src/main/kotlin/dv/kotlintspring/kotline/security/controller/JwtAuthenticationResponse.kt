package dv.kotlintspring.kotline.security.controller


data class JwtAuthenticationResponse(
        var token: String? = null
)