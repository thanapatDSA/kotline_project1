package dv.kotlintspring.kotline.security.repository

import dv.kotlintspring.kotline.security.entity.Authority
import dv.kotlintspring.kotline.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository


interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}