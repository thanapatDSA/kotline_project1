package dv.kotlintspring.kotline.security.repository

import dv.kotlintspring.kotline.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}