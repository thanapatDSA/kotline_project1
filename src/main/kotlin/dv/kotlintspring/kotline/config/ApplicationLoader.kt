package dv.kotlintspring.kotline.config

import dv.kotlintspring.kotline.entity.*
import dv.kotlintspring.kotline.repository.*
import dv.kotlintspring.kotline.security.entity.Authority
import dv.kotlintspring.kotline.security.entity.AuthorityName
import dv.kotlintspring.kotline.security.entity.JwtUser
import dv.kotlintspring.kotline.security.repository.AuthorityRepository
import dv.kotlintspring.kotline.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        var manu3 = manufacturerRepository.save(Manufacturer("Samsung", "555666777888"))

        var product1 = productRepository.save(Product("CAMT",
                "The best College in CMU", 0.0,1,
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product2 = productRepository.save(Product("iPhone",
                "It's a phone",28000.00,20,
                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(Product("Prayuth",
                "The best PM ever",1.00,1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        var product4 = productRepository.save(Product("Note 9",
                "Other Iphone",28001.00,10,
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))

        var customer1 = customerRepository.save(Customer("Lung","pm@go.th",UserStatus.ACTIVE))
        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย"," เขตดุสิต","แขวง ดินสอ","กรุงเทพ","10123"))

        var customer2 = customerRepository.save(Customer("ชัชชาติ","chut@taopoon.com",UserStatus.ACTIVE))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่"," ต.สุเทพ ","อ.เมือง","จ.เชียงใหม่","50200"))

        var customer3 = customerRepository.save(Customer("ธนาธร","thanathorn@life.com",UserStatus.PENDING))
        var address3 = addressRepository.save(Address("ซักที่บนโลก "," ต.สุขสันต์ ","อ.ในเมือง","จ.ขอนแก่น","12457"))

        var shoppingCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var selectedProduct1 = selectedProductRepository.save(SelectedProduct(product2,4))
        var selectedProduct2 = selectedProductRepository.save(SelectedProduct(product3,1))

        var shoppingCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
        var selectedProduct3 = selectedProductRepository.save(SelectedProduct(product3,1))
        var selectedProduct4 = selectedProductRepository.save(SelectedProduct(product1,1))
        var selectedProduct5 = selectedProductRepository.save(SelectedProduct(product4,2))

        manu1.products.add(product1)
        product1.manufacturer = manu1
        manu2.products.add(product2)
        product2.manufacturer = manu2
        manu1.products.add(product3)
        product3.manufacturer = manu1
        manu3.products.add(product4)
        product4.manufacturer = manu3

        customer1.defaultAddress = address1
        customer2.defaultAddress = address2
        customer3.defaultAddress = address3

        shoppingCart1.customer = customer1
        shoppingCart1.selectedProducts.add(selectedProduct1)
        shoppingCart1.selectedProducts.add(selectedProduct2)
        shoppingCart2.customer = customer2
        shoppingCart2.selectedProducts.add(selectedProduct3)
        shoppingCart2.selectedProducts.add(selectedProduct4)
        shoppingCart2.selectedProducts.add(selectedProduct5)

        dataLoader.loadData()
        loadUsernameAndPassword()
    }

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val custl = Customer(name = "สมชาติ", email = "a@b.com")
        val cusJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = custl.email,
                enabled = true,
                firstname = custl.name,
                lastname = "unknown"
        )
        customerRepository.save(custl)
        userRepository.save(cusJwt)
        custl.jwtUser = cusJwt
        cusJwt.user = custl
        cusJwt.authorities.add(auth2)
        cusJwt.authorities.add(auth3)
        cusJwt.authorities.add(auth1)
    }

}