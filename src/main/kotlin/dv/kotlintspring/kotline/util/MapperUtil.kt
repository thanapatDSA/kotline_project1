package dv.kotlintspring.kotline.util

import dv.kotlintspring.kotline.dao.ManufacturerDao
import dv.kotlintspring.kotline.entity.*
import dv.kotlintspring.kotline.entity.dto.*
import dv.kotlintspring.kotline.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(Mapping(source = "manufacturer", target = "manu"))
    fun mapProductDto(product: Product?): ProductDto?

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    fun mapProductDto(product: List<Product>): List<ProductDto>
    fun mapManufacturer(manu: Manufacturer): ManufacturerDto
    fun mapManufacturer(manu: List<Manufacturer>): List<ManufacturerDto>
    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @Mappings(Mapping(source = "defaultAddress", target = "addressCus"),
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities"))
//    fun mapCustomer(customer: Customer?): CustomerDto
    fun mapCustomerDto(customer: Customer?): CustomerDto
    fun mapCustomerDto2(customer: Customer?): CustomerDto2
    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>


    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    fun mapCustomerDto(customer: List<Customer>): List<CustomerDto>
    fun mapAddress(addressCus: Address): AddressDto
    @InheritInverseConfiguration
    fun mapAddress(addressCus: AddressDto): Address

    @Mappings(
            Mapping(source = "selectedProducts", target = "selectProductsShop"),
            Mapping(source = "customer", target = "customerShop"))
    fun mapShoppingCartDto(shoppingcart: ShoppingCart): ShoppingCartDto

    fun mapShoppingCartDto(shoppingcart: List<ShoppingCart>): List<ShoppingCartDto>
    fun mapSelectProductShop(selectProductsShop: SelectedProduct): SelectedProductDto
    fun mapSelectProductShop(selectProductsShop: List<SelectedProduct>): List<SelectedProductDto>

//    @Mappings(
//            Mapping(source = "customer.jwtUser.username", target = "username"),
//            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
//    )
//    fun mapCustomer(customer: Customer?): CustomerDto
//
//    fun mapAuthority(authority: Authority): AuthorityDto
//    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

}