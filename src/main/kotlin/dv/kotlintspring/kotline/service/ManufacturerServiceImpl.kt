package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.dao.ManufacturerDao
import dv.kotlintspring.kotline.entity.Manufacturer
import dv.kotlintspring.kotline.entity.dto.ManufacturerDto
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl : ManufacturerService {

    override fun save(manu: ManufacturerDto): Manufacturer {
        val menufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(menufacturer)
    }


    @Autowired
    lateinit var manufacturerDao: ManufacturerDao

    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufactrers()
    }
}