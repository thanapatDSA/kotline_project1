package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.entity.ShoppingCart
import dv.kotlintspring.kotline.entity.dto.ShoppingCartDto
import dv.kotlintspring.kotline.entity.dto.ShoppingCustomerDto
import org.springframework.data.domain.Page

interface ShoppingCartService{
    fun getShoppingCarts():List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartProductNamePartilWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun save(customerId: Long, shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart
}