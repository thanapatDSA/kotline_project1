package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.dao.SelectedProductDao
import dv.kotlintspring.kotline.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectProductServiceImpl: SelectProductService{
    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getProductWithPage(name,page,pageSize)
    }
    @Autowired
    lateinit var selectedProductDao : SelectedProductDao
}