package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.entity.Manufacturer
import dv.kotlintspring.kotline.entity.dto.ManufacturerDto


interface ManufacturerService{
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer
}