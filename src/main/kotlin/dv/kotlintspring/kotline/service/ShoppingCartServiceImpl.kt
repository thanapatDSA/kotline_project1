package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.dao.CustomerDao
import dv.kotlintspring.kotline.dao.ProductDao
import dv.kotlintspring.kotline.dao.SelectedProductDao
import dv.kotlintspring.kotline.dao.ShoppingCartDao
import dv.kotlintspring.kotline.entity.SelectedProduct
import dv.kotlintspring.kotline.entity.ShoppingCart
import dv.kotlintspring.kotline.entity.ShoppingCartStatus
import dv.kotlintspring.kotline.entity.dto.ShoppingCustomerDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service

class ShoppingCartServiceImpl: ShoppingCartService{

    @Transactional
    override fun save(customerId: Long, shoppingCustomerDto: ShoppingCustomerDto): ShoppingCart {
        val customer = customerDao.findById(customerId)
        val shoppingCart = ShoppingCart(
                status = ShoppingCartStatus.WAIT
        )

        for(each in shoppingCustomerDto.products!!){
            var product = each.id?.let { productDao.findById(it) }
            var selectedProduct = SelectedProduct(product,each.quantity)
            selectedProduct.product = product
            selectedProductDao.save(selectedProduct)
            shoppingCart.selectedProducts.add(selectedProduct)
        }

        shoppingCart.customer = customer
        shoppingCartDao.save(shoppingCart)

        return shoppingCart


    }

    override fun getShoppingCartProductNamePartilWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartProductNamePartilWithPage(name,page,pageSize)
    }

    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartWithPage(page,pageSize)
    }
//    @Autowired
//    lateinit var shoppingCustomerDto : ShoppingCustomerDto

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
    @Autowired
    lateinit var productDao: ProductDao
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}