package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.dao.AddressDao
import dv.kotlintspring.kotline.dao.CustomerDao
import dv.kotlintspring.kotline.dao.ShoppingCartDao
import dv.kotlintspring.kotline.entity.Customer
import dv.kotlintspring.kotline.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service

class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    override fun save(addressId: Long, customer: Customer): Customer {
        var address = addressDao.findById(addressId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }

    override fun save(customer: Customer): Customer {
        var address = customer.defaultAddress?.let{addressDao.save(it)}
        val customer = customerDao.save(customer)
        return customer
    }

    override fun getCustomerBoughtProduct(name: String): List<Customer?> {
        return shoppingCartDao.getCustomerBoughtProduct(name)
                .map{shoppingCart -> shoppingCart.customer}.toSet().toList()
    }

    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

    override fun getCustomerByAddress(name: String): List<Customer> {
        return customerDao.getCustomerByAddress(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }
    @Autowired
    lateinit var addressDao: AddressDao
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerDao.getCustomersByName(name)
    }

}