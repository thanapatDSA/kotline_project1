package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.entity.Customer
import dv.kotlintspring.kotline.entity.UserStatus

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(name: String): List<Customer>
    fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer>
    fun getCustomerBoughtProduct(name: String): List<Customer?>
    fun save(customer: Customer): Customer
    fun save(addressId: Long, customer: Customer): Customer
    fun remove(id: Long): Customer?
}