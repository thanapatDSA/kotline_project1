package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.dao.AddressDao
import dv.kotlintspring.kotline.entity.Address
import dv.kotlintspring.kotline.entity.dto.AddressDto
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl:AddressService{
    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(address)
    }
    @Autowired
    lateinit var addressDao: AddressDao
}