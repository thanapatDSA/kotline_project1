package dv.kotlintspring.kotline.service


import dv.kotlintspring.kotline.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectProductService {
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}
