package dv.kotlintspring.kotline.service

import dv.kotlintspring.kotline.entity.Address
import dv.kotlintspring.kotline.entity.dto.AddressDto

interface AddressService {
    fun save(address: AddressDto): Address

}