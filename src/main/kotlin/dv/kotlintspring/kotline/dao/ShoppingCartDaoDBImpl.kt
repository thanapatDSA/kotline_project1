package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.ShoppingCart
import dv.kotlintspring.kotline.repository.ShoppingCartRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShoppingCartDaoDBImpl: ShoppingCartDao{
    override fun save(shoppingCart: ShoppingCart): ShoppingCart {
        return shoppingCartRepository.save(shoppingCart)
    }

    override fun getCustomerBoughtProduct(query: String): List<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(query)
    }

    override fun getShoppingCartProductNamePartilWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findBySelectedProducts_Product_NameContainingIgnoreCase(name, PageRequest.of(page,pageSize))
    }

    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartRepository.findAll(PageRequest.of(page, pageSize))
    }

    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartRepository.findAll() as List<ShoppingCart>
    }
}