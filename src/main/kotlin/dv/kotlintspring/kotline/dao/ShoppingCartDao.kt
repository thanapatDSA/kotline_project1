package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao{
    fun getShoppingCarts():List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartProductNamePartilWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerBoughtProduct(query: String):List<ShoppingCart>
    fun save(shoppingCart: ShoppingCart):ShoppingCart
}