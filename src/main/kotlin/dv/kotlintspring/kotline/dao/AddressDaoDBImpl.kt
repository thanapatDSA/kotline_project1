package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Address
import dv.kotlintspring.kotline.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressDaoDBImpl:AddressDao{
    override fun findById(addressId: Long): Address? {
        return addressRepository.findById(addressId).orElse(null)
    }

    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository
}