package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.SelectedProduct
import dv.kotlintspring.kotline.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class SelectedProductDaoDBImpl: SelectedProductDao{
    override fun save(selectedProduct: SelectedProduct): SelectedProduct {
        return selectedProductRepository.save(selectedProduct)
    }

    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameContainingIgnoreCase(name, PageRequest.of(page, pageSize))
    }

    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
}