package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Manufacturer
import dv.kotlintspring.kotline.entity.dto.ManufacturerDto
import dv.kotlintspring.kotline.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository


@Profile("db")
@Repository
class ManufacturerDaoDBImpl : ManufacturerDao {
    override fun getManufactrers(): List<Manufacturer> {
        return  manufacturerRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

    override fun findById(manuId: Long): Manufacturer? {
        return manufacturerRepository.findById(manuId).orElse(null)
    }

    override fun save(manu: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manu)
    }




    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
}

