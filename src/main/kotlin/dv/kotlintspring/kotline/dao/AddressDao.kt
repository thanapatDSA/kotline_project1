package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Address

interface AddressDao {
    fun save(address: Address): Address
    fun findById(addressId: Long): Address?
}