package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Customer
import dv.kotlintspring.kotline.entity.UserStatus
import dv.kotlintspring.kotline.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBlmpl: CustomerDao{
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }

    override fun getCustomerByAddress(name: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomersByName(name: String): Customer? {
        return customerRepository.findByName(name)
    }
}