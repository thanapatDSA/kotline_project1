//package dv.kotlintspring.kotline.dao
//
//
//import dv.kotlintspring.kotline.entity.Manufacturer
//import org.springframework.stereotype.Repository
//
//@Repository
//class ManufacturerDaoImpl : ManufacturerDao {
//    override fun getManufacturers(): List<Manufacturer> {
//        return mutableListOf(Manufacturer("Apple", "053123456"),
//                Manufacturer("Samsung", "555666777888"),
//                Manufacturer("CAMT", "0000000000"))
//    }
//}