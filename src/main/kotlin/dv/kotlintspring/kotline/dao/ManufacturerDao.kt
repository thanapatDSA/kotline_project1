package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Manufacturer
import dv.kotlintspring.kotline.entity.dto.ManufacturerDto


interface ManufacturerDao {
//    fun getManufacturers(): List<Manufacturer>
    fun save(manu: Manufacturer): Manufacturer

    fun findById(manuId: Long): Manufacturer?
    fun getManufactrers(): List<Manufacturer>
}