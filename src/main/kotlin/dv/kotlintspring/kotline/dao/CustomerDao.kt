package dv.kotlintspring.kotline.dao

import dv.kotlintspring.kotline.entity.Customer
import dv.kotlintspring.kotline.entity.UserStatus

interface CustomerDao {
    fun getCustomers(): List<Customer>
    fun getCustomersByName(name: String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByAddress(name: String): List<Customer>
    fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?
}