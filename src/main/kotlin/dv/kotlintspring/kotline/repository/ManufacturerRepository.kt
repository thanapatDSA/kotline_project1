package dv.kotlintspring.kotline.repository

import dv.kotlintspring.kotline.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository : CrudRepository<Manufacturer, Long> {
    fun findByName(name: String): Manufacturer
}