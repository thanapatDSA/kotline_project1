package dv.kotlintspring.kotline.repository

import dv.kotlintspring.kotline.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository: CrudRepository<ShoppingCart,Long>{
    fun findAll(page: Pageable):Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name:String,page: Pageable):Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String):List<ShoppingCart>
    fun findByCustomer_Id(id: Long): ShoppingCart
}