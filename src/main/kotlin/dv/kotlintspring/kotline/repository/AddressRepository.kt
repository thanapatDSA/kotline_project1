package dv.kotlintspring.kotline.repository

import dv.kotlintspring.kotline.entity.Address
import org.springframework.data.repository.CrudRepository

interface AddressRepository: CrudRepository<Address,Long>