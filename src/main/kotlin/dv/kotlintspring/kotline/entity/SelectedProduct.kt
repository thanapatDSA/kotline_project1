package dv.kotlintspring.kotline.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class SelectedProduct(
        @OneToOne var product: Product? = null,
        var quantity: Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

}