package dv.kotlintspring.kotline.entity

import javax.persistence.*

@Entity
data class ShoppingCart(var status:ShoppingCartStatus = ShoppingCartStatus.WAIT){
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var selectedProducts =  mutableListOf<SelectedProduct>()
    @OneToOne
    var customer: Customer? = null

}