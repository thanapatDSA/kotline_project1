package dv.kotlintspring.kotline.entity.dto

data class PageSelectedProductDto(var totalPage: Int? = null,
                          var totalElement: Long? = null,
                          var products: List<SelectedProductDto> = mutableListOf())