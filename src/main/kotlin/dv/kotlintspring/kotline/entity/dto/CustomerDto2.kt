package dv.kotlintspring.kotline.entity.dto

import dv.kotlintspring.kotline.entity.UserStatus

data class CustomerDto2(
        var name: String? = null,
        var email: String? = null,
        var userStatus: UserStatus? = null,
        var addressCus: AddressDto? = null,
        var id:Long? = null,
        var username: String? = null)