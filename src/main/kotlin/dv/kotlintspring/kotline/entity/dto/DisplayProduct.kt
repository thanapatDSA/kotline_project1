package dv.kotlintspring.kotline.entity.dto

data class DisplayProduct(var id: Long? = null,
                          var name: String? =null,
                          var description: String? = null,
                          var quantity: Int? = null)
