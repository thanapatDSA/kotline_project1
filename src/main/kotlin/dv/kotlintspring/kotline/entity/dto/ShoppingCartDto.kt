package dv.kotlintspring.kotline.entity.dto

import dv.kotlintspring.kotline.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var status: ShoppingCartStatus? = null,
        var selectProductsShop: List<SelectedProductDto>? = null,
        var customerShop: CustomerDto2? = null
)