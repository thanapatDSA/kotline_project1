package dv.kotlintspring.kotline.entity.dto

data class SelectedProductDto(
        var product: ProductDto? = null,
        var quantity: Int? = null
)