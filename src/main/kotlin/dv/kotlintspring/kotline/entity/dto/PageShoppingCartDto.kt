package dv.kotlintspring.kotline.entity.dto

data class PageShoppingCartDto(var totalPage: Int? = null,
                               var totalElement: Long? = null,
                               var item:List<ShoppingCartDto> = mutableListOf())