package dv.kotlintspring.kotline.entity.dto

import dv.kotlintspring.kotline.entity.Address

data class ShoppingCustomerDto(var customer: String? = null,
                               var defaultAddress: AddressDto? = null,
                               var products: List<DisplayProduct>? =null)
