package dv.kotlintspring.kotline.entity.dto

data class PageProductDto(var totalPage: Int? = null,
                          var totalElement: Long? = null,
                          var products: List<ProductDto> = mutableListOf())