package dv.kotlintspring.kotline.entity


enum class ShoppingCartStatus{
    WAIT,
    CONFIRM,
    PAID,
    SENT,
    RECEIVED
}