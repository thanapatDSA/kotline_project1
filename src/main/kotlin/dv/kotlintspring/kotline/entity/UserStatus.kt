package dv.kotlintspring.kotline.entity


enum class UserStatus {
    PENDING, ACTIVE, NOTACTIVE, DELETED
}