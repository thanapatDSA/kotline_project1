package dv.kotlintspring.kotline.entity

import javax.persistence.*

@Entity
data class Customer(
        override var name: String? = null,
        override var email: String? = null,
        override var userStatus: UserStatus? = null,
        var isDeleted : Boolean = false
) : User(name , email , userStatus) {
    @ManyToMany
    var shippingAddress = mutableListOf<Address>()
    @ManyToOne
    var billingAddress: Address? = null
    @OneToOne
    var defaultAddress: Address? = null
}



