package dv.kotlintspring.kotline.Controller

import dv.kotlintspring.kotline.entity.Person
import dv.kotlintspring.kotline.entity.myPersons
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController {
    @GetMapping("/helloWorld")
    fun getHelloWorld(): String {
        return "HelloWorld"
    }

    @GetMapping("/test")
    fun getTest(): String {
        return "CAMT"
    }

    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any> {
        val person = Person("somchai", "somrak", 15)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/myPerson")
    fun getMyPerson(): ResponseEntity<Any> {
        val myperson = Person("thanapat", "panyo", 22)
        return ResponseEntity.ok(myperson)
    }

    @GetMapping("/persons")
    fun getPersons(): ResponseEntity<Any> {
        val person01 = Person("somchai", "somrak", 15)
        val person02 = Person("Prayut", "Chan", 62)
        val person03 = Person("Lung", "Pom", 65)
        val persons = listOf<Person>(person01, person02, person03)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/myPersons")
    fun getMyPersons(): ResponseEntity<Any> {
        val myperson01 = myPersons("Osora", "Tsubasa", "Nachansu", 10)
        val myperson02 = myPersons("Hyuoka", "Kojiro", "Meiwa", 9)
        val mypersons = listOf<myPersons>(myperson01, myperson02)
        return ResponseEntity.ok(mypersons)
    }

    @GetMapping("/params")
    fun getParams(@RequestParam("name") name: String, @RequestParam("surname") surnname: String)
            : ResponseEntity<Any> {
        return ResponseEntity.ok("$name $surnname")
    }

    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParm(@PathVariable("name") name: String,
                    @PathVariable("surname") surname: String,
                    @PathVariable("age") age: Int): ResponseEntity<Any> {
        val person = Person(name, surname, age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo(@RequestBody person: Person): ResponseEntity<Any>{
        return ResponseEntity.ok(person)
    }
}

