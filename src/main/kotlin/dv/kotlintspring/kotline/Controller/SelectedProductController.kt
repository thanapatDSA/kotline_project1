package dv.kotlintspring.kotline.Controller


import dv.kotlintspring.kotline.entity.dto.PageProductDto
import dv.kotlintspring.kotline.entity.dto.PageSelectedProductDto
import dv.kotlintspring.kotline.service.SelectProductService
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectProductService

    @GetMapping("/slectedProduct/name")
    fun getProductWithPage(@RequestParam("name") name: String,
                           @RequestParam("page") page: Int,
                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any>
    {
        val output = selectedProductService.getProductWithPage(name, page, pageSize)
//        return ResponseEntity.ok(output.content)
        return ResponseEntity.ok(PageSelectedProductDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                products = MapperUtil.INSTANCE.mapSelectProductShop(output.content)))
    }
}