package dv.kotlintspring.kotline.Controller

import dv.kotlintspring.kotline.entity.dto.AddressDto
import dv.kotlintspring.kotline.service.AddressService
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class AddressController {
    @Autowired
    lateinit var addressService: AddressService

    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }

    @PutMapping("/address")
    fun updateAddress(@RequestBody address: AddressDto): ResponseEntity<Any> {
        if (address.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id mustnot be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )
    }

    @PutMapping("/address/{addressId}")
    fun updateAddressBypath(@PathVariable("addressId") id: Long?,
                            @RequestBody address: AddressDto): ResponseEntity<Any> {
        address.id = id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(
                        addressService.save(address)
                )
        )

    }
}