package dv.kotlintspring.kotline.Controller

import dv.kotlintspring.kotline.entity.Customer
import dv.kotlintspring.kotline.entity.UserStatus
import dv.kotlintspring.kotline.entity.dto.CustomerDto
import dv.kotlintspring.kotline.service.CustomerService
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController{
    @Autowired
    lateinit var customerService: CustomerService

    @GetMapping("/customer")
    fun getCustomer(): ResponseEntity<Any>{
        var customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomerByName(@RequestParam("name")name:String):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
    @GetMapping("/customer/partialQuery")
    fun getCustomersPartial(@RequestParam("name")name:String,
                            @RequestParam(value = "email", required = false) email: String?):ResponseEntity<Any> {
        var output:List<CustomerDto> = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/addressName")
    fun getCustomerByAddressName(@RequestParam("province")name: String):ResponseEntity<Any>{
        val  output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByAddress(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerByStatus(@RequestParam("status")status: UserStatus): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByStatus(status)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/product")
    fun getCustomerBoughtProduct(@RequestParam("name")name:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerBoughtProduct(name))
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer")
    fun addProduct(@RequestBody customerDto: CustomerDto):ResponseEntity<Any>{
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/{adressId}")
    fun addProductWithManufacturerExistd(@RequestBody customerDto: CustomerDto,
                   @PathVariable adressId:Long):ResponseEntity<Any>{
        val output = customerService.save(adressId
                ,MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }
}

private fun Any.mapCustomerDto(customerBoughtProduct: List<Customer?>): Any {
return listOf(customerBoughtProduct)
}
