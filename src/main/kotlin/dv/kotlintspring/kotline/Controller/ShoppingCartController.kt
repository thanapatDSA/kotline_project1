package dv.kotlintspring.kotline.Controller

import dv.kotlintspring.kotline.entity.dto.DisplayProduct
import dv.kotlintspring.kotline.entity.dto.PageShoppingCartDto
import dv.kotlintspring.kotline.entity.dto.ShoppingCartDto
import dv.kotlintspring.kotline.entity.dto.ShoppingCustomerDto
import dv.kotlintspring.kotline.service.ShoppingCartService
import dv.kotlintspring.kotline.util.MapperUtil
import org.apache.commons.math3.stat.descriptive.summary.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingcart")
    fun getShoppingCart(): ResponseEntity<Any> {
        var shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingcart/page")
    fun getShoppingCartWithPage(@RequestParam("page") page: Int,
                                @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        var output = shoppingCartService.getShoppingCartWithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                item = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingcart/Product")
    fun getShoppingCartProductNamePartilWithPage(@RequestParam("name") name: String,
                                                 @RequestParam("page") page: Int,
                                                 @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        var output = shoppingCartService.getShoppingCartProductNamePartilWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                item = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @PostMapping("/shoppingcart/addShoppingcarts/{customerId}")
    fun addShoppingCartByCustomerId(@PathVariable customerId: Long
                                    , @RequestBody shoppingCustomerDto: ShoppingCustomerDto): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(
                shoppingCartService.save(customerId,shoppingCustomerDto))

        var displayProduct = mutableListOf<DisplayProduct>()
        for (each in output.selectProductsShop!!) {
            displayProduct.add(DisplayProduct(name = each.product?.name,
                    id = each.product?.id,
                    description = each.product?.description, quantity = each?.quantity))
        }
        return ResponseEntity.ok(ShoppingCustomerDto(customer = output.customerShop?.name,
                defaultAddress = output.customerShop?.addressCus, products = displayProduct))



    }
}