package dv.kotlintspring.kotline.Controller

import dv.kotlintspring.kotline.entity.dto.ManufacturerDto
import dv.kotlintspring.kotline.service.ManufacturerService
import dv.kotlintspring.kotline.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ManufacturerController {
    @Autowired
    lateinit var manufacturerService: ManufacturerService

    @GetMapping("/manufacturer")
    fun getAllManufacturer(): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapManufacturer(manufacturerService.getManufacturers()))
    }

    @PostMapping("/manufacturer")
    fun addManufacturer(@RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer(manufacturerService.save(manu))
        )
    }

    @PutMapping("/manufacturer")
    fun updateManufacturer(@RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        if (manu.id == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("id mustnot be null")
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer((
                        manufacturerService.save(manu)
                        ))
        )
    }

    @PutMapping("/manufacturer/{manuId}")
    fun updateManufacturer(@PathVariable("manuId") Id: Long?,
                           @RequestBody manu: ManufacturerDto): ResponseEntity<Any> {
        manu.id = Id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapManufacturer((
                        manufacturerService.save(manu)
                        ))
        )
    }
}